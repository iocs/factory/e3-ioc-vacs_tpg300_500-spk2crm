require essioc
require vac_ctrl_tpg300_500

iocshLoad("${essioc_DIR}/common_config.iocsh")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-dtl-30-u006.tn.esss.lu.se")

epicsEnvSet("DEVICE_NAME", "Spk-020Crm:Vac-VEG-10001")
epicsEnvSet("MOXA_PORT", "4004")

iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_ctrl_tpg300_500_moxa.iocsh, "DEVICENAME = $(DEVICE_NAME), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = Spk-020Crm:Vac-VGC-10000, CHANNEL = A1, CONTROLLERNAME = $(DEVICE_NAME)")
dbLoadRecords("$(E3_CMD_TOP)/startScan.template", "DEVICENAME = Spk-020Crm:Vac-VGC-10000")
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = Spk-020Crm:Vac-VGC-20000, CHANNEL = B1, CONTROLLERNAME = $(DEVICE_NAME)")
dbLoadRecords("$(E3_CMD_TOP)/startScan.template", "DEVICENAME = Spk-020Crm:Vac-VGC-20000")

